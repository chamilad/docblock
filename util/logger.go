/*
Copyright 2019 chamilad

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package util

import (
	"github.com/sirupsen/logrus"
	"os"
)

var log *logrus.Logger = nil

func SetupLogging(debug bool) {
	log = logrus.New()
	log.Formatter = &logrus.TextFormatter{
		ForceColors:   true,
		FullTimestamp: true,
	}

	log.Out = os.Stdout
	if debug == true {
		log.Level = logrus.DebugLevel
	} else {
		log.Level = logrus.InfoLevel
	}
}

func GetLogger() *logrus.Logger {
	if log == nil {
		SetupLogging(false)
	}

	return log
}
