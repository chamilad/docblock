/*
Copyright 2019 chamilad

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package common

// Config file to be parsed
type SourceFile struct {
	FileName string
	Path     string
	Elements []*Element
}

// Config Element
type Element struct {
	Name          string
	Comment       *Comment
	ChildElements []*Element
	Attributes    []*Attribute
	Value         string
}

type Comment struct {
	Description string
	Fields      map[string]string
}

// XML attribute. TODO: need to move this to xml specific reading and generalize Element
type Attribute struct {
	Name  string
	Value string
}

// Parsing Language definition
type ParsingLanguage struct {
	Name     string            `yaml:"name"`
	KeyStone string            `yaml:"keystone"`
	Fields   map[string]string `yaml:"fields"`
	Output   struct {
		Nulls struct {
			Description string `yaml:"description"`
			Fields      string `yaml:"fields"`
		} `yaml:"nulls"`
	} `yaml:"output"`
}

// DocBlockConfig is the configuration file format for the parser to read parameters.
// These are read from the `conf.yml` file at the same location as the docblock binary.
type DocBlockConfig struct {
	ParserFormat        string `yaml:"parserFormat"`
	AllowEmptyDescripts bool   `yaml:"allowEmptyDescriptions"`
	Parsing             struct {
		Input struct {
			Type   string `yaml:"type"`
			Source struct {
				Location       string `yaml:"location"`
				Extension      string `yaml:"extension"`
				ExcludePattern string `yaml:"excludePattern"`
			} `yaml:"source"`
		} `yaml:"input"`
		Output struct {
			Type       string `yaml:"type"`
			Properties []struct {
				Key   string `yaml:"key"`
				Value string `yaml:"value"`
			}
			Nulls struct {
				Description string `yaml:"description"`
				Fields      string `yaml:"fields"`
			} `yaml:"nulls"`
			Path string `yaml:"path"`
		} `yaml:"output"`
	} `yaml:"parsing"`
}

// SupportedInputTypes is the array of supported input types by the docblock tool. Currently, only XML is supported.
var SupportedInputTypes = []string{
	"xml",
}

// SupportedOutputTypes is the array of supported output types by the docblock tool. Currently only markdown is output,
// along with markdown to HTML conversion.
var SupportedOutputTypes = []string{
	"markdown",
}
