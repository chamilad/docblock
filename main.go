/*
Copyright 2019 chamilad

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"bufio"
	"flag"
	"fmt"
	"github.com/sirupsen/logrus"
	"gitlab.com/chamilad/docblock/common"
	"gitlab.com/chamilad/docblock/parser/xml"
	"gitlab.com/chamilad/docblock/publisher/md"
	"gitlab.com/chamilad/docblock/util"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
)

var (
	log     *logrus.Logger
	Version string
	Build   string
)

func main() {
	// 1. Load tool
	// 1.1. Flags
	debugFlg := flag.Bool("v", false, "enable verbose output")
	versionFlg := flag.Bool("version", false, "show version details")

	// 1.1.2 Override usage text
	flag.Usage = func() {
		_, err := fmt.Fprintf(os.Stderr, "docblock %s build %s\n\n"+
			"docblock is a tool to parse and generate documentation from\n"+
			"config files. The documentation is collected from comments\n"+
			"inside the file. The comment should be in a specific format,\n"+
			"which can be defined using a parser \"language\". The parsed\n"+
			"content can be output into different formats such as\n"+
			"Markdown\n\n"+
			"The behavior of the tool is configured using the config\n"+
			"file \"conf.yml\". This	 file should be present in the root\n"+
			"of the current working directory.\n\n"+
			"Options to be used\n", Version, Build)

		util.CheckErr(err)
		flag.PrintDefaults()
	}

	flag.Parse()

	// 1.1.3 Show version details
	if *versionFlg {
		_, err := fmt.Fprintf(os.Stderr, "%s build %s\n", Version, Build)
		util.CheckErr(err)
		os.Exit(0)
	}

	// 1.2. Setup logging
	util.SetupLogging(*debugFlg)
	log = util.GetLogger()

	// 1.3. Parse config
	log.Info("loading docblock configuration...")
	var conf *common.DocBlockConfig
	conf = loadConfig("conf.yml")
	// TODO: check the need to validate the presence of each config option

	// 1.4. Parse language
	log.Info("setting parser language...")
	var lang *common.ParsingLanguage
	lang = loadParseLang(conf)

	// 1.5. Validate input config
	// 1.5.1. Input type inputSupported
	var inputSupported = false
	for _, sIType := range common.SupportedInputTypes {
		if strings.TrimSpace(conf.Parsing.Input.Type) == sIType {
			inputSupported = true
		}
	}

	if !inputSupported {
		log.Error("specified input type ", conf.Parsing.Input.Type, " is not supported.")
		os.Exit(1)
	}

	// Existence of the source location is checked at the actual file opening time

	// 1.5.2. Output type inputSupported
	var outputSupported = false
	for _, sOType := range common.SupportedOutputTypes {
		if strings.TrimSpace(conf.Parsing.Output.Type) == sOType {
			outputSupported = true
		}
	}

	if !outputSupported {
		log.Error("specified output type ", conf.Parsing.Output.Type, " is not supported.")
		os.Exit(1)
	}

	// TODO: validate the output type properties per publisher

	// 2. Collect files to parse
	log.Info("collecting files to parse in ", conf.Parsing.Input.Source.Location)
	// 2.1. Find files matching the provided extension in the source location. Files are looked for recursively.
	var sourceFiles []string
	var sourceDirs []string
	err := filepath.Walk(conf.Parsing.Input.Source.Location, func(path string, info os.FileInfo, err error) error {
		if info.IsDir() {
			sourceDirs = append(sourceDirs, path)
		} else {
			if filepath.Ext(path) == conf.Parsing.Input.Source.Extension && !strings.Contains(path, conf.Parsing.Input.Source.ExcludePattern) {
				sourceFiles = append(sourceFiles, path)
				log.Debug("collecting ", path, " to parse...")
			}
		}

		return nil
	})

	util.CheckErr(err)

	if len(sourceFiles) == 0 {
		log.Info("no files with extension ", conf.Parsing.Input.Source.Extension, " was found inside ", conf.Parsing.Input.Source.Location)
		os.Exit(0)
	}

	// 3. Setup output location
	// 3.1. Check if output path is set, if not use CWD
	rootOutputDir := ""
	if strings.TrimSpace(conf.Parsing.Output.Path) == "" {
		log.Info("output path not defined in config. using current working directory instead ")
		cwd, err := filepath.Abs(filepath.Dir(os.Args[0]))
		util.CheckErr(err)
		rootOutputDir = cwd
	} else {
		// TODO: check if target is relative. Has to be absolute until this is solved.
		rootOutputDir = conf.Parsing.Output.Path
	}

	// 3.2. Create the output directory
	conf.Parsing.Output.Path = fmt.Sprintf("%s/output/docs", rootOutputDir)
	log.Info("creating output directory: ", conf.Parsing.Output.Path)
	util.CheckErr(os.MkdirAll(conf.Parsing.Output.Path, 0777))

	// 3. For each source file collected, run the parser
	for _, sourceFilePath := range sourceFiles {
		// 3.1 Start building the SourceFile model, set file path values
		srcFile := new(common.SourceFile)

		// Figure out filepath relative to the source path configured in the conf.yml
		confSrcAbsPath, _ := filepath.Abs(conf.Parsing.Input.Source.Location)
		fileAbsPath, _ := filepath.Abs(sourceFilePath)
		relativeFilePath := strings.Replace(fileAbsPath, confSrcAbsPath, "", -1)
		pathSeparator := fmt.Sprintf("%c", os.PathSeparator)
		relativeFilePath = strings.TrimPrefix(relativeFilePath, pathSeparator)
		srcFile.FileName = relativeFilePath

		srcFile.Path = sourceFilePath

		// 3.2. Process file
		processFile(conf, srcFile, lang)
	}

	log.Info("done")
	os.Exit(0)
}

// Parse a configuration file for documentation.
func processFile(conf *common.DocBlockConfig, srcFile *common.SourceFile, lang *common.ParsingLanguage) {
	log.Debug("parsing file ", srcFile.Path, " of type ", conf.Parsing.Input.Type, " to type ", conf.Parsing.Output.Type, " in ", conf.Parsing.Output.Path)

	// Open the source file to parse
	file, err := os.Open(srcFile.Path)
	util.CheckErr(err)

	// Mark for file closure
	defer file.Close()

	fileReader := bufio.NewReader(file)

	// Check file type and parse
	switch conf.Parsing.Input.Type {
	case "xml":
		srcFile.Elements = xml.ParseFile(fileReader, lang)
	}

	// Write to the output type
	switch conf.Parsing.Output.Type {
	case "markdown":
		md.Publish(srcFile, lang, conf)
	}
}

// Load docblock configuration.
func loadConfig(confPath string) *common.DocBlockConfig {
	conf := new(common.DocBlockConfig)
	log.Debug("reading config file ", confPath, "...")
	confData, err := ioutil.ReadFile(confPath)
	util.CheckErr(err)

	log.Debug("parsing config file...")
	err = yaml.Unmarshal(confData, conf)
	util.CheckErr(err)

	return conf
}

// Parse language definition for doc comment.
func loadParseLang(conf *common.DocBlockConfig) *common.ParsingLanguage {
	langFile := "lang/" + conf.ParserFormat + ".yml"
	log.Debug("reading language file ", langFile, "...")
	langData, err := ioutil.ReadFile(langFile)
	util.CheckErr(err)

	lang := new(common.ParsingLanguage)
	log.Debug("parsing lang file...")
	err = yaml.Unmarshal(langData, &lang)
	util.CheckErr(err)

	// Load defaults
	if lang.Output.Nulls.Description == "" {
		log.Debug("did not find a value for output.nulls.description in the lang. using the specified default: ", conf.Parsing.Output.Nulls.Description)
		lang.Output.Nulls.Description = conf.Parsing.Output.Nulls.Description
	}

	if lang.Output.Nulls.Fields == "" {
		log.Debug("did not find a value for output.nulls.fields in the lang. using the specified default: ", conf.Parsing.Output.Nulls.Fields)
		lang.Output.Nulls.Fields = conf.Parsing.Output.Nulls.Fields
	}

	return lang
}

// TODO: YAML parsing with comment preservation is only available in python - https://pypi.python.org/pypi/ruamel.yaml
// could try with Go's AST
