/*
Copyright 2019 chamilad

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package md

import (
	"bytes"
	"fmt"
	"github.com/russross/blackfriday"
	"gitlab.com/chamilad/docblock/common"
	"gitlab.com/chamilad/docblock/util"
	"io/ioutil"
	"math/rand"
	"os"
	"strings"
	"text/template"
)

// needed to keep the anchor ID of the topmost element of the page
var topAnchor int64

// TODO: need to figure out better formatting for multiline continued comments.
// TODO: if input type is xml, there will be one root element, and the next children should be in the menu
func Publish(srcFile *common.SourceFile, lang *common.ParsingLanguage, conf *common.DocBlockConfig) {
	log := util.GetLogger()
	log.Info("generating Markdown output for source file ", srcFile.FileName)

	// Write the Markdown format
	childSections := []string{}
	//markdownContent := fmt.Sprintf("# %s\n", srcFile.FileName)
	markdownContent := ""
	// TODO: introduce a file-wide description that is not bound to an element
	for index, root := range srcFile.Elements {
		childSection, _ := describeElement(root, 0, fmt.Sprintf("%d", index+1), lang)
		childSections = append(childSections, childSection)
	}

	for _, childSection := range childSections {
		markdownContent += "\n\n" + childSection
	}

	// Write above result to file

	// Replace any path seperatator characters in the path with dashes
	outputFilename := strings.Replace(srcFile.Path, fmt.Sprintf("%c", os.PathSeparator), "-", -1)
	// Formulate file path to be written
	outputFileMd := fmt.Sprintf("%s/%s.md", conf.Parsing.Output.Path, outputFilename)
	log.Debug("writing Markdown content to file at ", outputFileMd)
	err := ioutil.WriteFile(outputFileMd, []byte(markdownContent), 0644)
	util.CheckErr(err)

	// Check if HTML conversion is needed and do so
	for _, outputProperty := range conf.Parsing.Output.Properties {
		if outputProperty.Key == "publishHTML" && outputProperty.Value == "true" {
			log.Info("publishing HTML from Markdown is enabled (publishHTML: true). Generating HTML content for source file ", srcFile.FileName)

			outputFilename := strings.Replace(srcFile.Path, fmt.Sprintf("%c", os.PathSeparator), "-", -1)
			outputFileHtml := fmt.Sprintf("%s/%s.md.html", conf.Parsing.Output.Path, outputFilename)
			htmlContent := string(blackfriday.MarkdownCommon([]byte(markdownContent)))

			//Changes in the generated HTML content to suit styling.
			htmlContent = strings.Replace(htmlContent, "<table>", "<table class=\"table-sm table-hover table-bordered\">", -1)
			htmlContent = strings.Replace(htmlContent, "<thead>", "<thead class=\"thead-default\">", -1)

			// Data model for HTML templating
			type OutputModel struct {
				FileName string
				Content  string
			}

			outputModel := OutputModel{FileName: srcFile.FileName, Content: htmlContent}

			var b bytes.Buffer
			t, err := template.ParseFiles("publisher/md/templates/minimal.html")
			util.CheckErr(err)

			err = t.Execute(&b, &outputModel)
			util.CheckErr(err)

			log.Debug("writing HTML content to file ", outputFileHtml)
			err = ioutil.WriteFile(outputFileHtml, b.Bytes(), 0644)
			util.CheckErr(err)
		}
	}
}

func describeElement(root *common.Element, depth int, numbering string, lang *common.ParsingLanguage) (string, int64) {
	childSections := []string{}
	sectionId := rand.Int63n(100000000000)

	// if this is the first child being processed, store the anchor ID
	if depth == 0 {
		topAnchor = sectionId
	}

	// HTML doesn't have header levels more than 6. Flooring down any depth larger than 4 (+2) to 4.
	if depth > 4 {
		depth = 4
	}

	hDepth := strings.Repeat("#", depth+2)
	childOutput := fmt.Sprintf("<span class=\"anchor\" id=\"%d\">&nbsp;</span> \n%s %s. <code>&lt;%s&gt;</code>\n\n", sectionId, hDepth, numbering, root.Name)
	elementDescr := lang.Output.Nulls.Description
	if root.Comment != nil {
		elementDescr = root.Comment.Description
	}

	childOutput += util.EscapeSpecialChars(elementDescr) + "\n\n"

	// if this is one of the child elements being processed, add a "back to top" link on the right side
	if depth > 0 {
		childOutput += fmt.Sprintf("<p class=\"text-right font-weight-bold float-right\"><a href=\"#%d\">&#9650; Back to top</a></p>\n\n", topAnchor)
	}

	if len(root.ChildElements) > 0 {
		childOutput += "**Configurations**\n\n"

		// Table heading
		// standard columns
		childOutput += "| Element        | Attributes |  Description         | Default Value |"

		// language specific columns
		customFieldKeys := util.GetCustomFieldKeys(lang)

		for _, fieldKeys := range customFieldKeys {
			childOutput += fmt.Sprintf(" %s |", lang.Fields[fieldKeys])
		}

		childOutput += "\n"

		// colunm alignment details
		childOutput += "| :------------: |------------|----------------------|---------------|"
		childOutput += strings.Repeat("-------------|", len(lang.Fields)) + "\n"

		// populate rows
		for cIndex, element := range root.ChildElements {
			elementDescr := lang.Output.Nulls.Fields
			if element.Comment != nil {
				elementDescr = element.Comment.Description
			}

			// Child elements of this element
			childSection, childSectionId := describeElement(element, depth+1, fmt.Sprintf("%s.%d", numbering, cIndex+1), lang)
			childSections = append(childSections, childSection)

			// Attributes
			attribs := lang.Output.Nulls.Fields
			if len(element.Attributes) > 0 {
				attribs = ""
				for _, attrib := range element.Attributes {
					attribs += fmt.Sprintf("`%s`, ", attrib.Name)
				}
				attribs = strings.TrimSuffix(attribs, ", ")
			}

			var defaultValue string
			if len(element.ChildElements) > 0 {
				defaultValue = fmt.Sprintf("[composite](#%d)", childSectionId)
			} else {
				defaultValue = element.Value
			}

			// populate values in standard columns
			childOutput += fmt.Sprintf("| [`<%s>`](#%d) | %s | %s | %s |", element.Name, childSectionId, attribs, util.EscapeSpecialChars(elementDescr), defaultValue)

			// populate values in lang specific columns
			for _, fieldKey := range customFieldKeys {
				fieldValue := lang.Output.Nulls.Fields

				if element.Comment != nil {
					fieldValue = element.Comment.Fields[fieldKey]
					if len(fieldValue) == 0 {
						fieldValue = lang.Output.Nulls.Fields
					}
				}

				childOutput += fmt.Sprintf(" %s |", util.EscapeSpecialChars(fieldValue))
			}

			childOutput += "\n"
		}

		for _, childSection := range childSections {
			childOutput += "\n\n" + childSection
		}
	}

	return childOutput, sectionId
}
