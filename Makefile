# Copyright 2019 chamilad
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# name of the binary
BINARY=docblock

# version and build number
VERSION=0.1
BUILD=`git rev-parse --short HEAD`

# pass version and build number when compiling the source
LDFLAGS=-ldflags "-extldflags '-static' -X main.Version=${VERSION} -X main.Build=${BUILD}"

# set default goal
.DEFAULT_GOAL: $(BINARY)

# clean target directory, build statically linked binary, and copy the binary to target directory
$(BINARY): clean
	mkdir -p target
	go build -race ${LDFLAGS} -o target/${BINARY}-${VERSION}-${BUILD}
	chmod 700 target/${BINARY}-${VERSION}-${BUILD}

# run go tests and produce code coverage reports
test: clean
	mkdir -p target/coverage/
	go tool vet -composites=false -shadow=true *.go
	# executing script to run commands otherwise not possible in makefile
	./coverage.sh

# populate the distribution structure and compress to a zip file
# also genenrate the sha256 checksum
install: $(BINARY)
	mkdir target/dist
	cp target/${BINARY}-${VERSION}-${BUILD} target/dist/${BINARY}
	cp -r resources/* target/dist/
	# TODO: temporary until direct HTML support is implemented
	rm -rf target/dist/publisher/html
	cp README.md target/dist/
	cp USAGE.md target/dist/
	cd target/dist/; zip -r ../${BINARY}-${VERSION}-${BUILD}.zip *
	cd target/; shasum ${BINARY}-${VERSION}-${BUILD}.zip > ${BINARY}-${VERSION}-${BUILD}.zip.txt; shasum -c ${BINARY}-${VERSION}-${BUILD}.zip.txt

# run a crude integration test
integration: install
	mkdir -p /tmp/docblock-test/integration/
	unzip target/docblock-*.zip -d /tmp/docblock-test/integration/
	sed -i 's/location: ".\/"/location: ".\/samples\/"/g' /tmp/docblock-test/integration/conf.yml
	cd /tmp/docblock-test/integration/; ./docblock -v

# clear the target directory
clean:
	go clean
	rm -rf target
	rm -rf /tmp/docblock-test






