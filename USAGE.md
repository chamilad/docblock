# Usage Guide
This document contains the detailed steps on how to configure and run DocBlock for a single project. 

## Download
Download the latest release from the [GitLab releases page](https://gitlab.com/chamilad/docblock/tags). Unzip the downloaded `.zip` file to a suitable location. This will contain the following artifacts.

1. DocBlock binary `docblock`
2. DocBlock Configuration file `conf.yml`
3. Standard and sample Parser language definitions `lang/*.yml`
4. HTML templates to generate HTML documentation `publisher/md/templates/minimal.html`
5. Sample DocBlock documented XML configuration files `samples/**/*.xml`

```
├── conf.yml
├── docblock
├── lang
│   ├── acme.yml
│   └── standard.yml
├── publisher
│   └── md
│       └── templates
│           └── minimal.html
├── README.md
└── samples
    ├── api-manager.xml
    └── dir1
        └── sample-conf.xml

```

## Configuration
The configuration for the DocBlock execution is contained inside the configuration file `conf.yml`. It contains the following options

### `parserFormat`
This defines the parser language (format) that should be used for the particular project. DocBlock ships with a default `standard` language. The definition for this parser format is contained inside `lang/standard.yml`.  If a new parser format is defined, the filename of the parser language should be specified as the value for `parserFormat` in `conf.yml`. 

#### Creating a Parser Format
A parser format is the definition of the DocBlock parseable comment format. This contains,

1. `keystone` - The word that DocBlock uses as the que to consider a particular comment as a DocBlock comment. For `standard` parser format, this is the word `doc`, which means that any comment that starts with `@doc` will be considered as a documentation comment.
2. `fields` - The list of fields (in addition to the description) that DocBlock should look for in the documentation comment. Each `field` entry is a key-value pair. The key is the actual field name to be used inside the documentation comment. The value is the pretty name that should be used when generating documentation. For `standard` parser format, there are two such fields, `type` and `possible_values`. 

For `standard` parser format, a typical documentation comment should look like the following.

```xml
<!-- @doc
This is a descriptive comment for a following element
@type String
@possible_values a list of hostnames
-->
<!-- <hostnames>someotherhostname </hostnames> -->
<hostnames>hostnames</hostnames>
```

Notice that each field is prefixed by `@` character. 

The above documentation comment will be parsed as the following table, when the documentation is generated.

```
# -----------------------------------------------------------------------------------
# | Element   | Attributes | Description             | Type   | Possible Values     |
# -----------------------------------------------------------------------------------
# |           |            | This is a descriptive   |        |                     |
# |<hostnames>|      -     | comment for a following | String | a list of hostnames |
# |           |            | element                 |        |                     |
# -----------------------------------------------------------------------------------

```

Additional fields can be defined to include more descriptive structured information per comment. 

### `allowEmptyDescriptions`
TODO:

### `parsing`
This section contains all the configurations to do with configuration file parsing.

#### `parsing.input`
This section contains all the configurations to do with input for the parser process.

##### `parsing.input.type`
This defines the type of configuration files that should be parsed. Currently, only type `xml` is supported. More types will be implemented in the future.

##### `parsing.input.source`
This section contains the information on the source configuration file location.

###### `parsing.input.source.location`
This defines the folder path from which DocBlock should look for configuration files. This can be either a location relative to the current working directory (e.g.: `./`) or an absolute path.

###### `parsing.input.source.extension`
This defines the extensions of the files that DocBlock should filter from the location defined in `parsing.input.source.location`. Only filenames ending with string `.<extension>` will be selected for parsing.

###### `parsing.input.source.excludePattern`
This defines any files that should be excluded from parsing, even after being selected through extension filtering defined in `parsing.input.source.extension`. For an example, any metadata XML files that should be excluded from parsing can be specified here. DocBlock will look for this pattern inside the full file path and exclude any filenames that contain this string in the file path.

#### `parsing.output`
This section contains all the configurations to do with documentation generation from the information parsed from the sources defined in `parsing.input`.

##### `parsing.output.type`
Defines the format the documentation to generated. Currently only `markdown` is supported as an output format. Markdown publisher will additionally generate HTML from Markdown content.

##### `parsing.output.properties`
This contains any custom properties that the publisher expects/can receive to be used during content generation. For Markdown there is one such property that can change the content generation behavior.

These properties should be defined as a YAML array of `key`, `value` pairs.

```yaml
properties:  
    - key: "property1"
      value: "value1"
    - key: "property2"
      value: "value2"
```

###### `markdown` Publisher Properties
1.  `publishHTML`
This property has two values, `true` or `false`. If set to `true`, Markdown publisher will also generate HTML content in addition to the Markdown content. If set to `false`, HTML content will not be generated.

##### `parser.output.nulls`
This section contains the content that should be used when empty values are found for standard elements of a documentation comment.

###### `parser.output.nulls.description`
This defines the content that should be used when an empty description is found in an element. 

###### `parser.output.nulls.fields`
This defines the content that should be used when an empty value is found for any of the parser format defined field.

##### `parser.output.path`
This defines the path into which the generated content should be written into. The path should be an absolute path. If the path value is empty, the current working directory is used. The generated content is put inside the path `output/docs` inside the specified (or derived) path.

## Comment Source Files
After the parser format is defined, the source files that are intended for this automation process should be commented with the proper format. To verify correct format, the tool can be executed and the resulting content can be checked.

## Execution
After the above configuration file is modified to suite the project needs the following simple invocation of the binary can be used to parse the intended source files.

```bash
./docblock
``` 

Use `-v` flag for debug output to be produced. 

This execution, if no error is found, will generate content in the output path inside the folders `output/docs/`. 

## Verification of Generated Content
Though DocBlock automates most of the content generation process, there may still be the requirement of scanning the generated content for incorrect output. Incorrect output may be generated as a result of incorrectly formatted documentation comments, non-standard use of XML or because of bugs in the tool. Therefore, it's recommended to scan the generated files and verify for correct content before hosting them for wider audience use.

If any errors are found in the generated content as a result of incorrectly formatted comments, correct and repeat from the above [Comment Source Files](#comment-source-files) section onwards, until the desired content is generated. 

