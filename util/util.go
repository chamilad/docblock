/*
Copyright 2019 chamilad

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package util

import (
	"gitlab.com/chamilad/docblock/common"
	"sort"
	"strings"
)

func CleanStringContent(content string) string {
	return strings.Trim(strings.TrimSpace(content), "\n")
}

// Check for returned errors
// TODO: remove for specific cases
func CheckErr(e error) {
	if e != nil {
		panic(e)
	}
}

// EscapeSpecialChars replaces the angle brackets and the ampersand chars in a given string with HTML escaped chars
func EscapeSpecialChars(rawValue string) string {
	// ampersand
	escapedValue := strings.Replace(rawValue, "&", "&amp;", -1)

	// opening angle bracket
	escapedValue = strings.Replace(escapedValue, "<", "&lt;", -1)

	// closing angle bracket
	escapedValue = strings.Replace(escapedValue, ">", "&gt;", -1)

	// replace \n\n with <br /><br />
	escapedValue = strings.Replace(escapedValue, "\n\n", "<br /><br />", -1)

	// all other line breaks are replaced with a space
	escapedValue = strings.Replace(escapedValue, "\n", "&nbsp;", -1)

	return escapedValue
}

// GetCustomFieldKeys returns a sorted consistently ordered list of field keys defined in the parser language.
// This is required since range map[string]string will not return a consistently ordered slice of keys all the time.
// The keys retrieved this way can be used to directly query for values in the map.
func GetCustomFieldKeys(lang *common.ParsingLanguage) []string {
	// 1. collect field names
	customFieldKeys := make([]string, 0)
	for fieldKey, _ := range lang.Fields {
		customFieldKeys = append(customFieldKeys, fieldKey)
	}
	// 2. sort by alphabetical order
	sort.Strings(customFieldKeys)

	return customFieldKeys
}
