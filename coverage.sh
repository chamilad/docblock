#!/usr/bin/env bash
# Copyright 2019 chamilad
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Generate code coverage reports for each package

PKG_LIST=$(go list ./... | grep -v /vendor/)
for package in ${PKG_LIST}; do
    go test -v -covermode=count -coverprofile "target/coverage/${package##*/}.cov" "$package"
done

# collect code coverage to a single coverage profile
tail -q -n +2 target/coverage/*.cov >> target/coverage/overall-coverage.cov
# add the mode line
sed -i '1 i\mode: count' target/coverage/overall-coverage.cov

# generate functional and html reports
go tool cover -func=target/coverage/overall-coverage.cov
go tool cover -html=target/coverage/overall-coverage.cov -o target/coverage.html
