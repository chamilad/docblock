/*
Copyright 2019 chamilad

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package parser

import (
	"fmt"
	"gitlab.com/chamilad/docblock/common"
	"gitlab.com/chamilad/docblock/util"
	"strings"
)

// TODO: should return whether a comment was found or not
func ParseComment(cmnt string, lang *common.ParsingLanguage) *common.Comment {
	// BUG(chamilad): TODO: identify and skip license header

	// trim leading and trailing spaces
	cmntString := strings.TrimSpace(cmnt)
	if len(cmntString) == 0 {
		return nil
	}

	// check if comment is a doc comment by checking existence of keystone word at the start
	if !strings.HasPrefix(cmntString, fmt.Sprintf("@%s", lang.KeyStone)) {
		return nil
	}

	// get all the fields by splitting the comment by @ sign
	cmntSections := strings.Split(cmntString, "@")

	// in a comment section
	// 0 - empty string before @ sign
	// 1 - keystone word and the element description
	// 2 (onwards) - fields

	comment := new(common.Comment)

	// first part of the comment section is the description, sans the keystone word
	comment.Description = util.CleanStringContent(strings.Split(cmntSections[1], lang.KeyStone)[1])

	// are there no fields?
	if len(cmntSections) == 2 {
		return comment
	}

	comment.Fields = make(map[string]string)
	// Collect the fields
	for i := 2; i < len(cmntSections); i++ {
		// a field consists of
		// <field keyword><space><field value>

		// split field string by space exactly once into two slices
		fieldSections := strings.SplitN(util.CleanStringContent(cmntSections[i]), " ", 2)

		// is this field keyword a valid one per the defined language?
		if _, ok := lang.Fields[fieldSections[0]]; ok {
			// then store the field value for field key
			comment.Fields[fieldSections[0]] = util.CleanStringContent(fieldSections[1])
		}
	}

	return comment
}
