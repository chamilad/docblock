# DocBlock
[![build status](https://gitlab.com/chamilad/docblock/badges/master/build.svg)](https://gitlab.com/chamilad/docblock/commits/master) 

[![coverage report](https://gitlab.com/chamilad/docblock/badges/master/coverage.svg)](https://gitlab.com/chamilad/docblock/commits/master)

## What?

DocBlock is a tool to define a documentation format for configuration files and to generate documentation from those configuration files.

Developers write comments in a pre-defined structure. Technical writers can then collect those comments in the configuration files into meaningful documentation by passing them through DocBlock.

The documentation format is configurable where the fields to be specified can be written in as a parser *language*. Therefore, DocBlock doesn't restrict users to a predefined set of fields or a format when it comes to documenting configuration. Separate projects can have their own documentation format. 

Currently, only `xml` is supported as a configuration file type. Markdown and Markdown based HTML documentation is generated.

![DocBlock Process](img/process.png)

## Why?

>tl;dr: Automation

Most usability issues arise from end users not properly understanding how to use the configuration files with the proper meaning of each configuration option. The common approach followed by most projects is to include the description of each configuration option in comments inside the configuration files. However, this results in a set of information that is not easily presentable or queryable. 

Presentable, queryable technical information is always produced by the technical writers. During the process in which the developers (who have the knowledge on the purpose of the configuration option) educate the technical writers (who have the knowledge on how to produce actual technical content), there is potential to lose meaning and content. 

The answer to these problems is automation. API documentation tackles this already with structured comments and annotations embedded in the code. The developers embed documentation oriented information in the code for tools later involved in the build process to extract them. The same can be done for the configuration files, where developers can write a block of structured comment for each configuration option in the configuration file. DocBlock will read and interpret them to automatically produce technical content. This content can then be reviewed by the technical writer expertise before being hosted publicly.

By structuring and automating the parsing phase, DocBlock tries to bridge a gap between the developer and the technical writer when it comes to user-facing configuration files.

## How?

>For the complete details of the tool, refer to [Usage Guide](USAGE.md)

>tl;dr: define a comment format, document the configuration files, integrate DocBlock

Normally a raw XML configuration file would look like the following.

```xml
<?xml version="1.0" encoding="utf-8" ?>
<root>
    <conf1>conf1</conf1>
    <conf2>
        <conf2.1>conf2.1</conf2.1>
        <conf2.2 attr="attribute">conf2.2</conf2.2>
        <!--<commentedConf>commented</commentedConf> -->
    </conf2>
</root>
```

With DocBlock you will be putting comments that describe the function of each configuration. For an example,

```xml
<?xml version="1.0" encoding="utf-8" ?>
<!-- @doc
Root configuration is the parent configuration for all options in this file.
-->
<root>
    <!-- @doc
    Conf1 controls functionality A

    @type string
    @possible_values conf1, conf2
    -->
    <conf1>conf1</conf1>
    <!-- @doc
    Conf2 contains configurations that control functionality B
    -->
    <conf2>
        <!-- @doc
        Conf 2.1 controls subfunctionality B.1

        @type boolean
        @possible_values true, false
        -->
        <conf2.1>false</conf2.1>
        <!-- @doc
        Conf 2.2 contains an attribute too

        @type string
        -->
        <conf2.2 attr="attribute">conf2.2</conf2.2>
        <!--<commentedConf>commented</commentedConf> -->
    </conf2>
</root>
```

When DocBlock operates on the above file, it would generate an output similar to the following.

```bash
docblock
```

-----
<span class="anchor" id="47941512656">&nbsp;</span> 
## 1. <code>&lt;root&gt;</code>

Root configuration is the parent configuration for all options in this file.

**Configurations**

| Element        | Attributes |  Description         | Default Value | Possible Values | Type |
| :------------: |------------|----------------------|---------------|-------------|-------------|
| [`<conf1>`](#44562772862) |  -  | Conf1 controls functionality A | conf1 | conf1, conf2 | string |
| [`<conf2>`](#69203193667) |  -  | Conf2 contains configurations that control functionality B | [composite](#69203193667) |  -  |  -  |


<span class="anchor" id="44562772862">&nbsp;</span> 
### 1.1. <code>&lt;conf1&gt;</code>

Conf1 controls functionality A

<p class="text-right font-weight-bold float-right"><a href="#47941512656">&#9650; Back to top</a></p>



<span class="anchor" id="69203193667">&nbsp;</span> 
### 1.2. <code>&lt;conf2&gt;</code>

Conf2 contains configurations that control functionality B

<p class="text-right font-weight-bold float-right"><a href="#47941512656">&#9650; Back to top</a></p>

**Configurations**

| Element        | Attributes |  Description         | Default Value | Possible Values | Type |
| :------------: |------------|----------------------|---------------|-------------|-------------|
| [`<conf2.1>`](#74225087444) |  -  | Conf 2.1 controls subfunctionality B.1 | false | true, false | boolean |
| [`<conf2.2>`](#89577219757) | `attr` | Conf 2.2 contains an attribute too | conf2.2 |  -  | string |


<span class="anchor" id="74225087444">&nbsp;</span> 
#### 1.2.1. <code>&lt;conf2.1&gt;</code>

Conf 2.1 controls subfunctionality B.1

<p class="text-right font-weight-bold float-right"><a href="#47941512656">&#9650; Back to top</a></p>



<span class="anchor" id="89577219757">&nbsp;</span> 
#### 1.2.2. <code>&lt;conf2.2&gt;</code>

Conf 2.2 contains an attribute too

<p class="text-right font-weight-bold float-right"><a href="#47941512656">&#9650; Back to top</a></p>

-----

The Markdown content is also used to produce an HTML output.

![Markdown HTML](img/sample-conf-html.png)

-----
-----

## TODO
1. Add support for YAML configuration files (currently only supports XML files)
2. Parse comments in more meaningful ways (ex: skip license headers)
3. Add support for more output types (currently supports only Markdown and subsequent Markdown to HTML)

Refer to the issues list for more TODOs. 

## Bugs and Feature Gaps
1. DocBlock is not able to handle copyright statements at the top of the file
2. Multiple comments before a certain config element are not combined together
3. XML element multiplicity is not correctly handled
4. Describing XML attributes is not supported

Please feel free to file issues for any other bugs found during usage.

## License
DocBlock is licensed under Apache v2.0.
