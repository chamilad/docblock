/*
Copyright 2019 chamilad

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package stack

// Stack implementation
type Stack struct {
	Size     int
	Elements []interface{}
}

func (s *Stack) Push(value interface{}) {
	s.Elements = append(s.Elements, value)
	s.Size++
}

func (s *Stack) Pop() (value interface{}) {
	if s.Size > 0 {
		s.Size--
		value = s.Elements[s.Size]
		s.Elements = s.Elements[0:s.Size]
		return value
	} else {
		return nil
	}
}

func (s *Stack) Peek() (value interface{}) {
	if s.Size > 0 {
		return s.Elements[s.Size-1]
	} else {
		return nil
	}
}
