/*
Copyright 2019 chamilad

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package stack

import (
	"reflect"
	"testing"
)

func TestStack_Push(t *testing.T) {
	testStack := new(Stack)
	testStack.Push("firstElement")
	if testStack.Size != 1 {
		t.Errorf("Stack size is wrong %d:%d", 1, testStack.Size)
	}
}

func TestStack_Pop(t *testing.T) {
	testStack := new(Stack)
	testStack.Push("firstElement")
	if element, ok := testStack.Pop().(string); ok {
		if element != "firstElement" {
			t.Error("Did not pop the same element")
		}
	} else {
		t.Errorf("Did not pop the same type %s:%s", "string", reflect.TypeOf(element))
	}
}

func TestStack_Pop2(t *testing.T) {
	testStack := new(Stack)
	testStack.Push("firstElement")
	testStack.Push("secondElement")
	element, _ := testStack.Pop().(string)
	if element != "secondElement" {
		t.Errorf("Did not pop the same element %s:%s", "secondElement", element)
	}
}

func TestStack_Pop3(t *testing.T) {
	testStack := new(Stack)
	testStack.Push("firstElement")
	testStack.Pop()
	if testStack.Size != 0 {
		t.Errorf("Did not pop properly [size] %d:%d ", 0, testStack.Size)
	}
}

func TestStack_Pop4(t *testing.T) {
	testStack := new(Stack)
	testStack.Push("firstElement")
	element, _ := testStack.Pop().(string)
	if element != "firstElement" {
		t.Error("Did not pop the same element")
	}
}
func TestStack_Pop5(t *testing.T) {
	testStack := new(Stack)
	testStack.Push("firstElement")
	testStack.Push("secondElement")
	testStack.Pop()
	element, _ := testStack.Pop().(string)
	if element != "firstElement" {
		t.Error("Did not pop the same element")
	}
}

func TestStack_Peek(t *testing.T) {
	testStack := new(Stack)
	testStack.Push("firstElement")
	testStack.Push("secondElement")
	element, _ := testStack.Peek().(string)
	if element != "secondElement" {
		t.Errorf("Did not peek the same element %s:%s", "secondElement", element)
	}

	if testStack.Size != 2 {
		t.Errorf("Peek caused side effects %d:%d", 2, testStack.Size)
	}
}
