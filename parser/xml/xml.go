/*
Copyright 2019 chamilad

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Package xml provides documentation block comment parsing for XML configuration files.
package xml

import (
	"bufio"
	"encoding/xml"
	"fmt"
	"gitlab.com/chamilad/docblock/util"
	"strings"

	"gitlab.com/chamilad/docblock/common"
	"gitlab.com/chamilad/docblock/parser"
	"gitlab.com/chamilad/docblock/stack"
)

// ParseFile parses a given XML file for documentation comments.
// XML content is parsed as streaming XML where the events for element types will
// be checked for in each iteration of the string token.
//
// After combing through the file the found details are returned as an array of Element objects
// These can be considered as the list of root XML elements found in the XML file, though there
// should ideally only be one in an XML document.
func ParseFile(fileReader *bufio.Reader, lang *common.ParsingLanguage) []*common.Element {
	log := util.GetLogger()

	//Since XML comments come before the actual element, if a doc comment is found before the element
	// this flag makes sure the doc comment is attached to the correct subsequent element.
	commentFound := false
	var comment *common.Comment

	// A stack of elements is implemented to represent the depth of the XML document. For each level of depth
	// navigated, a pointer to the previous element is pushed to this stack. All closed elements are considered as
	// the children of the last element in the stack.
	// When a close element is found, the pointer is popped from the stack.
	elements := new(stack.Stack)
	var rootElements []*common.Element

	// Streaming XML parser
	decoder := xml.NewDecoder(fileReader)

	for {
		t, _ := decoder.Token()
		if t == nil {
			break
		}

		switch se := t.(type) {
		case xml.Comment:
			// Check if this is valid doc comment and parse it. The Comment object is attached
			// to returning object only after the subsequent element is found in the StartElement match.
			comment = parser.ParseComment(fmt.Sprintf("%s", se), lang)
			if comment != nil {
				commentFound = true
			} else {
				commentFound = false
			}
		case xml.StartElement:
			element := new(common.Element)
			element.Name = se.Name.Local
			if !commentFound {
				// There is an element without a doc comment.
				log.Debug("element without doc comment found: ", se.Name.Local)
				comment = nil
			}

			// Attach the earlier found doc comment to a Comment element
			element.Comment = comment

			// Look for XML attributes and attach to returning Element
			if len(se.Attr) > 0 {
				for _, attrib := range se.Attr {
					attr := new(common.Attribute)
					attr.Name = attrib.Name.Local
					attr.Value = attrib.Value
					element.Attributes = append(element.Attributes, attr)
				}
			}

			// Is this the root element of the XML file?
			if elements.Size == 0 {
				rootElements = append(rootElements, element)
			} else {
				// If not the root element, get the current parent element and register a child element
				parent, _ := elements.Pop().(*common.Element)
				parent.ChildElements = append(parent.ChildElements, element)
				elements.Push(parent)
			}

			// Attach element to element list
			elements.Push(element)

			// Reset values for the next iteration
			commentFound = false
			comment = nil
		case xml.EndElement:
			// All done with this parent element. Remove from element hierarchy.
			elements.Pop()
			comment = nil
			commentFound = false
		case xml.CharData:

			// has no child elements, just a value, store it as string
			valueBytes := xml.CharData(se)
			value := strings.TrimSpace(string([]byte(valueBytes)))
			value = strings.TrimPrefix(value, "\n")
			value = strings.TrimPrefix(value, "\r")
			value = strings.TrimPrefix(value, "\t")
			value = strings.TrimSuffix(value, "\n")
			value = strings.TrimSuffix(value, "\r")
			value = strings.TrimSuffix(value, "\r")

			if len(value) > 0 {
				element, _ := elements.Pop().(*common.Element)
				log.Debug(fmt.Sprintf("value found %s => %s", element.Name, value))
				element.Value = value
				elements.Push(element)
			}
		}
	}

	return rootElements
}
